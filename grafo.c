/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  change_me.c      - main file

  include your name there and license for distribution.

  Remove next text: This line should not appear in submitted
  work and project name should be change to match real application.
  If this text is there I want 10 points subtracted from final
  evaluation.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#define FPS 30.0
#define PORT 11221

#define CHOICE int
#define CHOICE_SINGLEPLAYER 0
#define CHOICE_MULTIPLAYER 1

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#include "mzapo_regs.h"
#include "grafyx.h"
#include "mzapo_parlcd.h"
#include "peryf.h"
#include "netoolz.h"
#include "utils.h"

typedef struct args {
  Grafyx* pGrafyx;
  SocketPckg* pRecv;
  uint8_t deviceID;
} args;

#define NETOOLZ

COLOR colorChoiceMenu(Peryf*);
CHOICE modeChoiceMenu(Peryf*);
int singlePlayerLoop(Peryf*);
int multiPlayerLoop(Peryf*);
void* threadReceiveCommands(/*Grafyx* grafyx, SocketPckg* recv*/ void* arguments);

int main(int argc, char *argv[])
{
  Peryf peryf;
  initPeryf(&peryf);
  
  CHOICE modeSelection = modeChoiceMenu(&peryf);
  if (modeSelection == CHOICE_MULTIPLAYER)
    multiPlayerLoop(&peryf);
  else if (modeSelection == CHOICE_SINGLEPLAYER)
    singlePlayerLoop(&peryf);
  
  return 0;
}

COLOR colorChoiceMenu(Peryf* peryf){  
  Grafyx grafyx;
  initGrafyx(&grafyx);

  #define COLOR_RECT_WIDTH 300
  #define COLOR_RECT_HEIGHT 60
  #define COLOR_RECT_TOP 30
  #define COLOR_RECT_GAP 30
  #define COLOR_RECT_STROKE_WIDTH 6
  
  while(!peryfGetKnobPressedG(peryf)){
    grafyxDrawBackground(&grafyx, BLACK);
    int rRectWidth = map(peryfGetKnobPositionR(peryf), 0, 255, 0, COLOR_RECT_WIDTH);
    int gRectWidth = map(peryfGetKnobPositionG(peryf), 0, 255, 0, COLOR_RECT_WIDTH);
    int bRectWidth = map(peryfGetKnobPositionB(peryf), 0, 255, 0, COLOR_RECT_WIDTH);

    grafyxSetNoFill(&grafyx);
    grafyxSetStrokeWidth(&grafyx, COLOR_RECT_STROKE_WIDTH);
    Rectangle colorRectOutline = {
      .left = (grafyx.bufWidth - COLOR_RECT_WIDTH) / 2,
      .top = COLOR_RECT_TOP,
      .width = COLOR_RECT_WIDTH,
      .height = COLOR_RECT_HEIGHT
    };
    grafyxDrawRect(&grafyx, colorRectOutline);
    colorRectOutline.top += colorRectOutline.height + COLOR_RECT_GAP;
    grafyxDrawRect(&grafyx, colorRectOutline);
    colorRectOutline.top += colorRectOutline.height + COLOR_RECT_GAP;
    grafyxDrawRect(&grafyx, colorRectOutline);

    Rectangle colorRectFill = {
      .left = (grafyx.bufWidth - COLOR_RECT_WIDTH) / 2 + COLOR_RECT_STROKE_WIDTH,
      .top = COLOR_RECT_TOP + COLOR_RECT_STROKE_WIDTH,
      .width = rRectWidth,
      .height = COLOR_RECT_HEIGHT - COLOR_RECT_STROKE_WIDTH
    };
    grafyxSetNoStroke(&grafyx);
    grafyxSetFill(&grafyx, RED);
    grafyxDrawRect(&grafyx, colorRectFill);
    colorRectFill.top += colorRectFill.height + COLOR_RECT_GAP;
    colorRectFill.width = gRectWidth;
    grafyxSetFill(&grafyx, GREEN);
    grafyxDrawRect(&grafyx, colorRectFill);
    colorRectFill.top += colorRectFill.height + COLOR_RECT_GAP;
    colorRectFill.width = bRectWidth;
    grafyxSetFill(&grafyx, BLUE);
    grafyxDrawRect(&grafyx, colorRectFill);

    grafyxRenderBuffer(&grafyx);

    int ledR = map(peryfGetKnobPositionR(peryf), 0, 255, 0, 75);
    int ledG = map(peryfGetKnobPositionG(peryf), 0, 255, 0, 75);
    int ledB = map(peryfGetKnobPositionB(peryf), 0, 255, 0, 75);
    peryfWriteLedRGB1(peryf, ledR << 16 | ledG << 8 | ledB);
  }
  // MAYBE WAIT UNTIL THE G BUTTON IS FULLY DEPRESSED IN SOME BUSY-WAIT LOOP ?
  while (peryfGetKnobPressedG(peryf));

  int r = map(peryfGetKnobPositionR(peryf), 0, 255, 0, 31);
  int g = map(peryfGetKnobPositionG(peryf), 0, 255, 0, 63);
  int b = map(peryfGetKnobPositionB(peryf), 0, 255, 0, 31);
  

  destroyGrafyx(&grafyx);
  return RGB(r, g, b);
}

CHOICE modeChoiceMenu(Peryf* peryf){
  Grafyx grafyx;
  initGrafyx(&grafyx);

  #define COLOR_RECT_WIDTH 300
  #define COLOR_RECT_HEIGHT 60
  #define COLOR_RECT_TOP 30
  #define COLOR_RECT_GAP 30
  #define COLOR_RECT_STROKE_WIDTH 6
  #define TEXT_Y_GAP 15
  #define TEXT_X_GAP 80

  CHOICE choice = CHOICE_SINGLEPLAYER;
  while(!peryfGetKnobPressedG(peryf)) {
    choice = (peryfGetKnobPositionG(peryf) / 4) % 2;

    grafyxSetNoFill(&grafyx);
    grafyxSetStrokeWidth(&grafyx, COLOR_RECT_STROKE_WIDTH);
    Rectangle choiceRectOutline = {
      .left = (grafyx.bufWidth - COLOR_RECT_WIDTH) / 2,
      .top = COLOR_RECT_TOP,
      .width = COLOR_RECT_WIDTH,
      .height = COLOR_RECT_HEIGHT
    };
    if (choice == CHOICE_SINGLEPLAYER)
      grafyxSetStroke(&grafyx, RED);
    else
      grafyxSetStroke(&grafyx, WHITE);
    grafyxDrawRect(&grafyx, choiceRectOutline);

    Point singlePlayerTextPoint = {
      .x = choiceRectOutline.left + COLOR_RECT_WIDTH / 2 - TEXT_X_GAP,
      .y = choiceRectOutline.top + COLOR_RECT_HEIGHT / 2 - TEXT_Y_GAP
    };
    grafyxSetFontScale(&grafyx, 2);
    grafyxDrawText(&grafyx, "Singleplayer", singlePlayerTextPoint);

    choiceRectOutline.top += choiceRectOutline.height + COLOR_RECT_GAP;
    if (choice == CHOICE_MULTIPLAYER)
      grafyxSetStroke(&grafyx, RED);
    else
      grafyxSetStroke(&grafyx, WHITE);
    grafyxDrawRect(&grafyx, choiceRectOutline);

    Point multiPlayerTextPoint = {
     .x = choiceRectOutline.left + COLOR_RECT_WIDTH / 2 - TEXT_X_GAP,
     .y = choiceRectOutline.top + COLOR_RECT_HEIGHT / 2 - TEXT_Y_GAP
    };
    grafyxDrawText(&grafyx, "Multiplayer", multiPlayerTextPoint);

    grafyxRenderBuffer(&grafyx);
  }
  // MAYBE WAIT UNTIL THE G BUTTON IS FULLY DEPRESSED IN SOME BUSY-WAIT LOOP ?
  while (peryfGetKnobPressedG(peryf));
  

  destroyGrafyx(&grafyx);
  return choice;
}

int multiPlayerLoop(Peryf* peryf){
  Grafyx grafyx;

  uint8_t deviceID = getRandomByte();
  SocketPckg recv;
  SocketPckg send;
  DataPckg recvData;
  DataPckg sendData;
  
  initGrafyx(&grafyx);
  broadSrcInit(&send, PORT);
  broadRcvInit(&recv, PORT);
  grafyxDrawBackground(&grafyx, BLACK);
  grafyxRenderBuffer(&grafyx);

  uint32_t cursorX, cursorY, prevCursorX, prevCursorY;
  prevCursorX = prevCursorY = cursorX = cursorY = 0;
  sendData.id = deviceID;

  int frameCount = 0;
  while (1){
    prevCursorX = cursorX;
    prevCursorY = cursorY;
    cursorX += getKnobOffsetR(peryfGetKnobPositionR(peryf));
    cursorY -= getKnobOffsetB(peryfGetKnobPositionB(peryf));
    
    if (peryfGetKnobPressedG(peryf)){
      while (peryfGetKnobPressedG(peryf));
      args args = {
        .pGrafyx = &grafyx,
        .pRecv = &recv,
        .deviceID = deviceID
      };
      pthread_t broadRcvThread;
      if (pthread_create(&broadRcvThread, NULL, &threadReceiveCommands, (void *)&args) != 0) {
        fprintf(stderr, "Uh-oh! Failed to instantiate thread!\n");
        return -1;
      }
      COLOR chosenStroke = colorChoiceMenu(peryf);
      grafyxSetStroke(&grafyx, chosenStroke);

      // dummy read & write to reset offsets
      getKnobOffsetR(peryfGetKnobPositionR(peryf));
      getKnobOffsetG(peryfGetKnobPositionG(peryf));
      getKnobOffsetB(peryfGetKnobPositionB(peryf));

      void* res;
      int s = pthread_cancel(broadRcvThread);
      if (s != 0)
          fprintf(stderr, "pthread_cancel failed with: %d\n", s);

      /* Join with thread to see what its exit status was */

      s = pthread_join(broadRcvThread, &res);
      if (s != 0)
          fprintf(stderr, "pthread_join failed with: %d\n", s);

      if (res != PTHREAD_CANCELED)
          fprintf(stderr, "thread wasn't canceled (shouldn't happen!)\n");
    }

    Point lineStart = {.x = prevCursorX, .y = prevCursorY};
    Point lineEnd = {.x = cursorX, .y = cursorY};

    if (lineStart.x != lineEnd.x || lineStart.y != lineEnd.y){
      sendData.xOrigin = prevCursorX;
      sendData.xDest = cursorX;
      sendData.yOrigin = prevCursorY;
      sendData.yDest = cursorY;
      sendData.color = grafyx.strokeColor;
      sendData.cmd = 0x00;
      broadSrcData(&send, &sendData);
    }
    grafyxDrawLine(&grafyx, lineStart, lineEnd);
    
    uint16_t colorBuffer = grafyx.strokeColor;
    while(broadRcvData(&recv, &recvData) == 20) {//zbavit se konstanty
      if(recvData.id != deviceID) {
        if (recvData.cmd == 0xff){
          grafyxDrawBackground(&grafyx, recvData.color);
        } else {
          Point lineOrigin = {.x = recvData.xOrigin, .y = recvData.yOrigin};
          Point lineDest = {.x = recvData.xDest, .y = recvData.yDest};
          grafyxSetStroke(&grafyx, recvData.color);
          grafyxDrawLine(&grafyx, lineOrigin, lineDest);
        }
      }
    }
    grafyxSetStroke(&grafyx, colorBuffer);
    grafyxRenderBuffer(&grafyx);

    peryfWriteLedLine(peryf, frameCount++);

    if (peryfGetKnobPressedR(peryf))
      break;
    if (peryfGetKnobPressedB(peryf)){
      sendData.cmd = 0xff;
      sendData.color = grafyx.strokeColor;
      broadSrcData(&send, &sendData);
      grafyxDrawBackground(&grafyx, sendData.color);
    }
    sleep(1.0 / FPS);
  }

  destroyGrafyx(&grafyx);
  return 0;
}

int singlePlayerLoop(Peryf* peryf){
  Grafyx grafyx;
  
  initGrafyx(&grafyx);
  grafyxDrawBackground(&grafyx, BLACK);
  grafyxRenderBuffer(&grafyx);

  uint32_t cursorX, cursorY, prevCursorX, prevCursorY;
  prevCursorX = prevCursorY = cursorX = cursorY = 0;

  int frameCount = 0;
  while (1){
    prevCursorX = cursorX;
    prevCursorY = cursorY;
    cursorX += getKnobOffsetR(peryfGetKnobPositionR(peryf));
    cursorY -= getKnobOffsetB(peryfGetKnobPositionB(peryf));
    
    if (peryfGetKnobPressedG(peryf)){
      while (peryfGetKnobPressedG(peryf));
      COLOR chosenStroke = colorChoiceMenu(peryf);
      grafyxSetStroke(&grafyx, chosenStroke);

      // dummy read & write to reset offsets
      getKnobOffsetR(peryfGetKnobPositionR(peryf));
      getKnobOffsetG(peryfGetKnobPositionG(peryf));
      getKnobOffsetB(peryfGetKnobPositionB(peryf));
    }

    Point lineStart = {.x = prevCursorX, .y = prevCursorY};
    Point lineEnd = {.x = cursorX, .y = cursorY};

    grafyxDrawLine(&grafyx, lineStart, lineEnd);
    
    grafyxRenderBuffer(&grafyx);

    peryfWriteLedLine(peryf, frameCount++);

    if (peryfGetKnobPressedB(peryf)){
      COLOR bgColor = grafyx.strokeColor;
      grafyxDrawBackground(&grafyx, bgColor);
    }

    if (peryfGetKnobPressedR(peryf))
      break;
  }

  destroyGrafyx(&grafyx);
  return 0;
}

void *threadReceiveCommands(/*Grafyx* grafyx, SocketPckg* recv*/ void* arguments){
  args* argS = (args*) arguments;
  Grafyx* grafyx = argS->pGrafyx;
  SocketPckg* recv = argS->pRecv;
  uint8_t deviceID = argS->deviceID;
  DataPckg recvData;
  while (1) {
    while(broadRcvData(recv, &recvData) == 20) {//zbavit se konstanty
      if(recvData.id != deviceID) {
        if (recvData.cmd == 0xff){
          grafyxDrawBackground(grafyx, recvData.color);
        } else {
          uint16_t colorBuffer = grafyx->strokeColor;
          Point lineOrigin = {.x = recvData.xOrigin, .y = recvData.yOrigin};
          Point lineDest = {.x = recvData.xDest, .y = recvData.yDest};
          grafyxSetStroke(grafyx, recvData.color);
          grafyxDrawLine(grafyx, lineOrigin, lineDest);
          grafyxSetStroke(grafyx, colorBuffer);
        }
      }
      printf("Thread going to sleep...\n");
      sleep(0.10); // cancelation happens here
    } 
  }
  return NULL;
}
