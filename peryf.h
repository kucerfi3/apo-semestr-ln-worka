typedef struct Peryf {
    unsigned char* peripheralsBase;
    uint32_t* spiLedLine;
    uint32_t* ledRGB1;
    uint32_t* ledRGB2;
    uint32_t* ledKBDWR_direct;
    uint32_t* spiKnobs_direct;
    uint32_t* spiKnobs;
} Peryf;

int initPeryf(Peryf*);

void peryfWriteLedLine(Peryf*, uint32_t);

void peryfWriteLedRGB1(Peryf*, uint32_t);

void peryfWriteLedRGB2(Peryf*, uint32_t);

int peryfGetKnobPositionR(Peryf*);

int peryfGetKnobPositionG(Peryf*);

int peryfGetKnobPositionB(Peryf*);

int peryfGetKnobPressedR(Peryf*);

int peryfGetKnobPressedG(Peryf*);

int peryfGetKnobPressedB(Peryf*);