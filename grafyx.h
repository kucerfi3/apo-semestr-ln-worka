#include "font_types.h"

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

#define DEBUG

// define Grafyx specific Boolean type
#define GBOOL signed char
// define common GBOOL values
#define GTRUE 1
#define GFALSE 0
#define GBOOL_UNITILIAZIED -128
#define GBOOL_MMAP_FAIL -1
#define GBOOL_ALLOC_FAIL -2

#define COLOR uint16_t

#define RGB(r,g,b) ( ((r) & 0b11111) << 11 | ((g) & 0b111111) << 5 | ((b) & 0b11111) )
#define R(color) (((color) >> 11) & 0b11111)
#define G(color) (((color) >> 5)  & 0b111111)
#define B(color) (((color) >> 0)  & 0b11111)
#define WHITE (RGB(31, 63, 31))
#define BLACK (RGB(0, 0, 0))
#define RED   (RGB(31, 0, 0))
#define GREEN (RGB(0, 63, 0))
#define BLUE  (RGB(0, 0, 31))

#define LOC(x, y, width) (((y) * (width)) + (x))

typedef struct Rectangle {
  int left;
  int top;
  int width;
  int height;
} Rectangle;

typedef struct Point {
  int x;
  int y;
} Point;

typedef struct Grafyx {
  unsigned char* lcdDisplay;
  #ifdef DEBUG
  GBOOL grafyxInitialized;
  #endif
  GBOOL strokeOn;
  GBOOL fillOn;
  uint32_t bufWidth;
  uint32_t bufHeight;
  uint32_t strokeWidth;
  uint32_t fontScale;
  COLOR* drawBuffer;
  COLOR strokeColor;
  COLOR fillColor;
  font_descriptor_t font;

} Grafyx;

/**
* Simple function returning 1 on Point being enclosed within rect and 0 if not.
*/
GBOOL isPointInRectangle(const Point*, const Rectangle*);

/**
* The first function that is to be called before Grafyx is utilized.
* Sets up Grafyx structure (Initlizes LCD display, allocates Pixel buffer etc.)
* Returns negative value upon failure and 0 otherwise
*/
GBOOL initGrafyx(Grafyx*);

/**
* The last function that is to be called after Grafyx is no longer needed.
* Basically frees all malloc'd memory.
*/
void destroyGrafyx(Grafyx*);

/**
* After all appropriate calls to Grafyx were made and drawBuffer has thus been filled,
* it's time to render it all onto LCD display via calling grafyxRenderBuffer and passing
* initialized Grafyx structure.
*/
void grafyxRenderBuffer(const Grafyx*);

/**
* Simple function to draw Rectangle to drawBuffer with dimension specified in 2nd argument.
*/
void grafyxDrawRect(const Grafyx*, const struct Rectangle);

/**
* Function to set fill color that is to be used to fill next Shape (rect, ellipse...)
*/
void grafyxSetFill(Grafyx*, const COLOR);

/**
* Function to set stroke color that is to be used to stroke next Shape (rect, ellipse...)
*/
void grafyxSetStroke(Grafyx*, const COLOR);

/**
* Function to set stroke width (in px) that is to be used to stroke next Shape (rect, ellipse...)
*/
void grafyxSetStrokeWidth(Grafyx*, const int);

/**
* Function to draw background - aka fill the screen with a single color specified by the 2nd argument
*/
void grafyxDrawBackground(Grafyx*, const COLOR);

/**
 * Function to draw text - aka print the string given in 2nd argument. 
 * 3rd argument specifies anchor point - top left corner.
 */
void grafyxDrawText(Grafyx*, const char*, const Point);

/**
 * Sets font size for upcoming text drawing. Value is given in 2nd argument.
 */
void grafyxSetFontScale(Grafyx*, const int);

/**
 * Sets typeface according to font descriptor in 2nd argument.
 */
void grafyxSetFont(Grafyx*, const font_descriptor_t);

/**
 * Next object will be drawn without stroke.
 */
void grafyxSetNoStroke(Grafyx*);

/**
 * Next object will be drawn without infill.
 */
void grafyxSetNoFill(Grafyx*);

/**
 * Will draw line from point A (2nd argument) to point B (3rd argument). Line will be drawn using stroke color.
 */
void grafyxDrawLine(Grafyx*, const Point, const Point);