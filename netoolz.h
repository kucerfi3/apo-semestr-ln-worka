#include <netinet/in.h>

typedef struct SocketPckg {
    int socketd;
    struct sockaddr_in addr;
} SocketPckg;

typedef struct DataPckg {
    uint8_t id;
    uint32_t xOrigin;
    uint32_t xDest;
    uint32_t yOrigin;
    uint32_t yDest;
    uint8_t cmd;
    uint16_t color;
} DataPckg;

/**
 * First function to be called. Initialises UDP broadcast socket for Sending.
 * 1st argument is socket descripor + adress struct, 2nd is desired port.
 * Returns negative value upon failure and 0 otherwise
 */
int broadSrcInit(SocketPckg*, uint16_t);

/**
 * First function to be called. Initialises UDP socket for Receiving.
 * 1st argument is socket descripor + adress struct, 2nd is desired port.
 * Returns negative value upon failure and 0 otherwise
 */
int broadRcvInit(SocketPckg*, uint16_t);

/**
 * Last function, that closes socket. Works for both receiving and sending.
 */
int connectionEnd(SocketPckg*);

/**
 * Sends bytes using previously initialised socket using broadSrcInit()
 * 1st argument is socket descripor + adress struct, 2nd is array with data to transmit, 
 * 3rd is length of the given message in bytes.
 * Returns negative value upon failure and 0 otherwise
 */
int broadSrcBytes(SocketPckg*, const signed char*, const uint16_t);

/**
 * Receives bytes using previously initialised socket using broadRcvInit()
 * 1st argument is socket descripor + adress struct, 2nd is array - buffer for data, 
 * 3rd is length of the given array in bytes.
 * Returns negative value upon failure and 0 otherwise
 */
int broadRcvBytes(SocketPckg*, signed char*, uint16_t);

/**
 * Sends grafo data using previously initialised socket using broadSrcInit()
 * 1st argument is socket descripor + adress struct, 2nd is grafo data pack, 
 * Returns negative value upon failure and 0 otherwise
 */
int broadSrcData(SocketPckg*, DataPckg*);

/**
 * Receives grafo data using previously initialised socket using broadRcvInit()
 * 1st argument is socket descripor + adress struct, 2nd is grafo data pack, 
 * Returns negative value upon failure and 0 otherwise
 */
int broadRcvData(SocketPckg*, DataPckg*);