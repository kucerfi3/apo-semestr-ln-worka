#include <stdint.h>
#include <stdio.h>

#include "mzapo_regs.h"
#include "peryf.h"
#include "mzapo_phys.h"

int initPeryf(Peryf* peryf){
    unsigned char* peripherals = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!peripherals)
        return -1;
    peryf->peripheralsBase = peripherals;

    peryf->spiLedLine = (uint32_t*) &peripherals[SPILED_REG_LED_LINE_o];
    peryf->ledRGB1 = (uint32_t*) &peripherals[SPILED_REG_LED_RGB1_o];
    peryf->ledRGB2 = (uint32_t*) &peripherals[SPILED_REG_LED_RGB2_o];
    peryf->ledKBDWR_direct = (uint32_t*) &peripherals[SPILED_REG_LED_KBDWR_DIRECT_o];
    peryf->spiKnobs_direct = (uint32_t*) &peripherals[SPILED_REG_KBDRD_KNOBS_DIRECT_o];
    peryf->spiKnobs = (uint32_t*) &peripherals[SPILED_REG_KNOBS_8BIT_o];

    return 0;
}

void peryfWriteLedLine(Peryf* peryf, uint32_t line){
    *peryf->spiLedLine = line;
}

void peryfWriteLedRGB1(Peryf* peryf, uint32_t rgb){
    if (rgb & 0xff000000)
        fprintf(stderr, "Warning: Writing more than 24 bits into RB1 LED. These bit are gonna be ignored.");
    *(peryf->ledRGB1) = rgb;
}

void peryfWriteLedRGB2(Peryf* peryf, uint32_t rgb){
    if (rgb & 0xff000000)
        fprintf(stderr, "Warning: Writing more than 24 bits into RB2 LED. These bit are gonna be ignored.");
    *(peryf->ledRGB2) = rgb;
}

int peryfGetKnobPositionR(Peryf* peryf){
    return (*(peryf->spiKnobs) & 0x00ff0000) >> 16;
}

int peryfGetKnobPositionG(Peryf* peryf){
    return (*(peryf->spiKnobs) & 0x0000ff00) >> 8;
}

int peryfGetKnobPositionB(Peryf* peryf){
    return *(peryf->spiKnobs) & 0x000000ff;
}

int peryfGetKnobPressedR(Peryf* peryf){
    return *(peryf->spiKnobs) & 0x04000000;
}

int peryfGetKnobPressedG(Peryf* peryf){
    return *(peryf->spiKnobs) & 0x02000000;
}

int peryfGetKnobPressedB(Peryf* peryf){
    return *(peryf->spiKnobs) & 0x01000000;
}

void peryfSetKeyboardStateLED1(Peryf* peryf, int state){
    if (state != 0 && state != 1)
        fprintf(stderr, "Warning: Setting state of keyboard LED1 to non boolean value. Result might be unexpected");
    if (state)
        *peryf->ledKBDWR_direct = *peryf->ledKBDWR_direct | 0x40;
    else
        *peryf->ledKBDWR_direct = *peryf->ledKBDWR_direct ^ 0x40;
    
}

void peryfSetKeyboardStateLED2(Peryf* peryf, int state){
    if (state != 0 && state != 1)
        fprintf(stderr, "Warning: Setting state of keyboard LED2 to non boolean value. Result might be unexpected");
    if (state)
        *peryf->ledKBDWR_direct = *peryf->ledKBDWR_direct | 0x80;
    else
        *peryf->ledKBDWR_direct = *peryf->ledKBDWR_direct ^ 0x80;
    
}