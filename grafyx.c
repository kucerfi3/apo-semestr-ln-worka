#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

//#include "font_types.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "mzapo_parlcd.h"
#include "grafyx.h"

#ifdef DEBUG
#define ERROR_MESSAGE_UNINITIALIZED "Grafyx Error! Library unitiliazized. Please, call initGrafyx(...) before using Grafyx functions.\nExiting program...\n"
#endif

// private
#ifdef DEBUG
GBOOL checkGrafyxInitialized(const Grafyx* grafyx){
  if(!grafyx->grafyxInitialized){
    fprintf(stderr, ERROR_MESSAGE_UNINITIALIZED);
    return GBOOL_UNITILIAZIED;
  }
  return GTRUE;
}
#endif


GBOOL isPointInRectangle(const Point* point, const Rectangle* rect){
  int xOff = point->x - rect->left;
  int yOff = point->y - rect->top;
  return (xOff >= 0 && xOff < rect->width) && (yOff >= 0 && yOff < rect->height);
}

GBOOL initGrafyx(Grafyx* grafyx){
  grafyx->bufWidth = LCD_WIDTH;
  grafyx->bufHeight = LCD_HEIGHT;

  unsigned char* lcdDisplay;
  lcdDisplay = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (!lcdDisplay)
    return GBOOL_MMAP_FAIL;
  parlcd_hx8357_init(lcdDisplay);

  grafyx->lcdDisplay = lcdDisplay;

  uint32_t bufSize = grafyx->bufWidth * grafyx->bufHeight;
  COLOR* buffer = (COLOR*) malloc(sizeof *buffer * bufSize);
  if (buffer == NULL)
    return GBOOL_ALLOC_FAIL;
  grafyx->drawBuffer = buffer;

  grafyx->strokeOn = grafyx->fillOn = GTRUE;
  grafyx->strokeWidth = 1;
  grafyx->strokeColor = grafyx->fillColor = WHITE;

  grafyx->font = font_rom8x16;
  grafyx->fontScale = 1;
  
  return grafyx->grafyxInitialized = GTRUE;
}

void destroyGrafyx(Grafyx* grafyx){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  free(grafyx->drawBuffer);
  grafyx->drawBuffer = NULL;
  grafyx->grafyxInitialized = GFALSE;
}

void grafyxRenderBuffer(const Grafyx* grafyx){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  uint32_t bufLen = grafyx->bufHeight * grafyx->bufWidth;

  parlcd_write_cmd(grafyx->lcdDisplay, 0x2c);
  for (int i = 0; i < bufLen; i++){
        parlcd_write_data(grafyx->lcdDisplay, grafyx->drawBuffer[i]);
    }
}

void grafyxDrawRectStroke(const Grafyx* grafyx, const Rectangle rect){
  const int rectRight = rect.left + rect.width;
  const int rectBottom = rect.top + rect.height;
  const int strokeWidth = grafyx->strokeWidth;

  // cache "optimized" top row(s)
  for (int y = rect.top; y < grafyx->bufHeight && y < rect.top + strokeWidth; y++)
    for (int x = rect.left; x < grafyx->bufWidth &&  x < rectRight; x++)
      grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;

  // bottom row(s)
  for (int y = rectBottom - strokeWidth; y < grafyx->bufHeight && y < rectBottom; y++)
    for (int x = rect.left; x < grafyx->bufWidth && x < rectRight; x++)
      grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;

  for (int y = rect.top + strokeWidth; y < grafyx->bufHeight && y < rectBottom - strokeWidth; y++){
    for (int x = rect.left; x < grafyx->bufWidth && x < rect.left + strokeWidth; x++)
      grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;

    for (int x = rectRight - strokeWidth; x < grafyx->bufWidth && x < rectRight; x++)
      grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;
  }
}

void grafyxDrawRectFill(const Grafyx* grafyx, const Rectangle rect){
  const int rectRight = rect.left + rect.width;
  const int rectBottom = rect.top + rect.height;
  const int strokeWidth = grafyx->strokeWidth;

  for (int y = rect.top + strokeWidth; y < grafyx->bufHeight && y < rectBottom - strokeWidth; y++)
    for (int x = rect.left + strokeWidth; x < grafyx->bufWidth && x < rectRight - strokeWidth; x++){
        grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->fillColor;
    }
}

void grafyxDrawRect(const Grafyx* grafyx, const Rectangle rect){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  if (grafyx->strokeOn)
    grafyxDrawRectStroke(grafyx, rect);
  
  if (grafyx->fillOn)
    grafyxDrawRectFill(grafyx, rect);
  
}

void grafyxSetFill(Grafyx* grafyx, const COLOR fillColor){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->fillOn = GTRUE;
  grafyx->fillColor = fillColor;
}

void grafyxSetStroke(Grafyx* grafyx, const COLOR strokeColor){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->strokeOn = GTRUE;
  grafyx->strokeColor = strokeColor;
}

void grafyxSetStrokeWidth(Grafyx* grafyx, const int strokeWidth){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->strokeWidth = strokeWidth;
}

void grafyxDrawBackground(Grafyx* grafyx, const COLOR bgColor){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  const int gBufLen = grafyx->bufHeight * grafyx->bufWidth;
  for (int i = 0; i < gBufLen; i++)
    grafyx->drawBuffer[i] = bgColor;
}

Point grafyxDrawChar(Grafyx* grafyx, const char c, const Point leftTop){
  const int bitsIdx = (c - grafyx->font.firstchar) * grafyx->font.height;
  int charWidth; 
  const int charScale = grafyx->fontScale;

  if (grafyx->font.width)
      charWidth = grafyx->font.width[c - grafyx->font.firstchar] * charScale;
  else
      charWidth = grafyx->font.maxwidth * charScale;

  const int bottomY = leftTop.y + grafyx->font.height * charScale;
  for (int y = leftTop.y; y < grafyx->bufHeight && y < bottomY; y++){
    const int rightX = leftTop.x + charWidth * charScale;
    const font_bits_t charPix = grafyx->font.bits[bitsIdx + (y - leftTop.y) / charScale];
    for (int x = leftTop.x; x < grafyx->bufWidth && x < rightX; x++){
      const int pixelOn = charPix & (1 << (16 - (x - leftTop.x) / charScale));
      if (pixelOn)
        grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;
    }
  }

  Point leftTopShifted = leftTop;
  leftTopShifted.x += charWidth;
  return leftTopShifted;
}

void grafyxDrawText(Grafyx* grafyx, const char* text, const Point leftTop){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  if (!grafyx->strokeOn)
    return;

  Point leftTopCur = leftTop;
  int textIdx = 0;
  char c = text[textIdx];
  while (c != '\0'){
    leftTopCur = grafyxDrawChar(grafyx, c, leftTopCur);
    
    c = text[++textIdx];
  }

}

void grafyxSetFontScale(Grafyx* grafyx, const int fontScale){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->fontScale = fontScale;
}

void grafyxSetFont(Grafyx* grafyx, const font_descriptor_t font){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->font = font;
}

void grafyxSetNoStroke(Grafyx* grafyx){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->strokeOn = GFALSE;
}

void grafyxSetNoFill(Grafyx* grafyx){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  grafyx->fillOn = GFALSE;
}

void grafyxDrawLine(Grafyx* grafyx, const Point A, const Point B){
  #ifdef DEBUG
  GBOOL retVal;
  if ( (retVal = checkGrafyxInitialized(grafyx)) != GTRUE)
    exit(retVal);
  #endif

  if (!grafyx->strokeOn)
    return;

  const int dY = B.y - A.y;
  const int dX = B.x - A.x;
  
  int steps;
  if (abs(dX) > abs(dY))
    steps = abs(dX);
  else
    steps = abs(dY);

  const float incX = dX / (float) steps;
  const float incY = dY / (float) steps;

  float x, y;
  x = A.x;
  y = A.y;

  for (int s = 0; s < steps; s++){
    x += incX;
    y += incY;
    if (y >= grafyx->bufHeight || x >= grafyx->bufWidth || x < 0 || y < 0)
      continue;
    grafyx->drawBuffer[LOC((int) x, (int) y, grafyx->bufWidth)] = grafyx->strokeColor;
  }
  /*const double incline = (double) dY / dX;

  int y = A.y;

  for (int x = A.x; x < grafyx->bufWidth && x < B.x; x++) {
    y = A.y + (x - A.x) * incline;
    if (y >= grafyx->bufHeight)
      continue;
    grafyx->drawBuffer[LOC(x, y, grafyx->bufWidth)] = grafyx->strokeColor;
  }*/
}