#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdint.h>

#include "netoolz.h"

#define PACKET_LEN 20


int connectionEnd(SocketPckg* pckg) {
    return close(pckg->socketd);
}

int broadInit(SocketPckg* pckg, uint16_t port, char mode) {
    pckg->socketd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
    if (pckg->socketd == -1) {
        fprintf(stderr, "Error creating socket\n%s\n", strerror(errno));
        return -1;
    }

    pckg->addr.sin_family = AF_INET;
    pckg->addr.sin_port = htons(port);
    if (mode) {
        pckg->addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
        int broadEnable = 1;
        if(setsockopt(pckg->socketd, SOL_SOCKET, SO_BROADCAST, &broadEnable, sizeof(broadEnable)) == -1) {
            fprintf(stderr, "Error setting broadcast (SOL_SOCKET, SO_BROADCAST)\n%s\n", strerror(errno));
            connectionEnd(pckg);
            return -1;
        }
    } else {
        pckg->addr.sin_addr.s_addr = INADDR_ANY;
        if(bind(pckg->socketd, (const struct sockaddr *) &(pckg->addr), sizeof(pckg->addr))) {
            fprintf(stderr, "Error binding\n%s\n", strerror(errno));
            connectionEnd(pckg);
            return -1;
        }
    }
    
    return 0;
}

int broadSrcInit(SocketPckg* pckg, uint16_t port) {
    return broadInit(pckg, port, 1);
}

int broadRcvInit(SocketPckg* pckg, uint16_t port) {
    return broadInit(pckg, port, 0);
}

int broadSrcBytes(SocketPckg* pckg, const signed char* data, const uint16_t len) {
    if (sendto(pckg->socketd, (const signed char *) data, len, 0, (const struct sockaddr *) &(pckg->addr), sizeof(pckg->addr)) == -1) {
        fprintf(stderr, "Error sending packet.\n%s\n", strerror(errno));
        return -1;
    }
    return 0;
}

int broadRcvBytes(SocketPckg* pckg, signed char data[], uint16_t data_len) {
    socklen_t len = 0;
    int n = recvfrom(pckg->socketd, (char *) data, data_len, MSG_WAITALL, 0, &len);
    if (n < 0) {
        //fprintf(stderr, "Error receiving.\n%s\n", strerror(errno));
        return -1;
    }
    data[n] = 0;
    return n;
}

int broadSrcData(SocketPckg* pckg, DataPckg* data) {

    int8_t buffer[PACKET_LEN];

    buffer[0] = data->id;
    buffer[1] = ((data->xOrigin >> 24) & 0xFF);
    buffer[2] = ((data->xOrigin >> 16) & 0xFF);
    buffer[3] = ((data->xOrigin >> 8) & 0xFF);
    buffer[4] = (data->xOrigin & 0xFF);
    buffer[5] = ((data->xDest >> 24) & 0xFF);
    buffer[6] = ((data->xDest >> 16) & 0xFF);
    buffer[7] = ((data->xDest >> 8) & 0xFF);
    buffer[8] = (data->xDest & 0xFF);
    buffer[9] = ((data->yOrigin >> 24) & 0xFF);
    buffer[10] = ((data->yOrigin >> 16) & 0xFF);
    buffer[11] = ((data->yOrigin >> 8) & 0xFF);
    buffer[12] = (data->yOrigin & 0xFF);
    buffer[13] = ((data->yDest >> 24) & 0xFF);
    buffer[14] = ((data->yDest >> 16) & 0xFF);
    buffer[15] = ((data->yDest >> 8) & 0xFF);
    buffer[16] = (data->yDest & 0xFF);
    buffer[17] = data->cmd;
    buffer[18] = ((data->color >> 8) & 0xFF);
    buffer[19] = (data->color & 0xFF);

    return broadSrcBytes(pckg, buffer, PACKET_LEN);
}

int broadRcvData(SocketPckg* pckg, DataPckg* data) {

    int8_t buffer[PACKET_LEN];

    int retval = broadRcvBytes(pckg, buffer, PACKET_LEN);

    data->id = buffer[0];
    data->xOrigin = (buffer[1] << 24) + (buffer[2] << 16) + (buffer[3] << 8) + buffer[4];
    data->xDest = (buffer[5] << 24) + (buffer[6] << 16) + (buffer[7] << 8) + buffer[8];
    data->yOrigin = (buffer[9] << 24) + (buffer[10] << 16) + (buffer[11] << 8) + buffer[12];
    data->yDest = (buffer[13] << 24) + (buffer[14] << 16) + (buffer[15] << 8) + buffer[16];
    data->cmd = buffer[17];
    data->color = (buffer[18] << 8) + buffer[19];

    return retval;
}
